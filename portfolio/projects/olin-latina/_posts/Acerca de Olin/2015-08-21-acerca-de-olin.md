---
title: Acerca de Olin
css: md-sub-styles
type: list
image: inscripcion-simultanea-md.jpg
category: Acerca de Olin
layout: detail
permalink: /acerca-de-olin/
description: Sobre Nosotros - Cada estudiante merece una oportunidad para sobre salir. Una oportunidad honesta y real para alcanzar su potencial.
---

# Acerca de Olin

### Mensaje del Director
¡Bienvenidos a Olin Virtual Academy! Como su director estoy orgulloso de dirigir una escuela que ofrece una oportunidad alternativa e innovadora para los estudiantes de preparatoria para tener éxito y obtener un diploma de escuela preparatoria. Somos una escuela preparatoria W.A.S.C. y AdvancEd Accredited escuela preparatoria 100% virtual.

Ofrecemos una amplia gama de cursos de recuperación, créditos A-G, y asignaturas optativas. Nuestro cuerpo estudiantil es diverso, con una variedad de estudiantes de todos los ámbitos de la vida de los cinco condados que inscribe: Los Angeles, Kern, Ventura, San Bernardino y Orange. Nuestros estudiantes aprenden a través de una combinación de software de base y de “sesiones en vivo” de tutoría en los cursos conectados a maestros con credenciales. A cada estudiante se le asigna un asesor de éxito de los estudiantes que funcionan como una “base” maestro de aula y apoyan al estudiante con la responsabilidad académica. Les damos una impresora portátil y servicio de internet gratuito para los estudiantes de familias de bajos ingresos. Los estudiantes que tienen éxito en Olin Virtual Academy se jactan de la fuerte comunicación con el personal y la voluntad de todos para apoyarlos. Únase a nosotros hoy y podremos empezar a moverlo hacia delante.

**Mr. José Salas**

### Misión de la escuela
Cada estudiante merece la oportunidad de sobresalir, una oportunidad honesta y real para alcanzar su potencial. En Olin Virtual Academy, nuestra misión es ayudar a los estudiantes en edad de escuela secundaria a desarrollar su potencial individual. El modelo de enseñanza y aprendizaje en línea de Olin seguirá aprovechando, habilidades de aprendizaje, basadas en las investigaciones del siglo 21 con el fin de crear, aprendices auto-motivados competentes, de toda la vida para que participen de manera efectiva en la sociedad. Vamos a seguir proporcionando una excelente alternativa de educación para los miles de estudiantes en edad de escuela secundaria en el condado de Los Angeles y los condados adyacentes que, por diversas razones, no asisten a la escuela secundaria, o pueden beneficiarse de una escuela no tradicional.

* ![Personal y Administración]({{ site.baseurl }}/images/staff-125x125.jpg)
* ### Nuestros Maestros
* Olin Virtual Academy emplea a los maestros más eficaces con la unidad para ayudar a los estudiantes a tener éxito.
* [Conozca a los Maestros]({{ site.baseurl }}/acerca-de-olin/maestros)

<!--section-->

* ![Personal y Administración]({{ site.baseurl }}/images/maestros-125x125.jpg)
* ### Personal y Administración
* La academia virtual Olin se mantiene unida por personal y administración.  
* [Personal y Administración]({{ site.baseurl }}/acerca-de-olin/personal)
