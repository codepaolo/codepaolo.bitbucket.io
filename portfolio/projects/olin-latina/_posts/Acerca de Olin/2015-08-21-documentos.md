---
title: Consejo Olin
css: md-sub-styles
type: list
image:
category: Acerca de Olin
layout: detail
permalink: /acerca-de-olin/consejo-olin/
description: Documentos Oficiales de la Academia Virtual Olin
---

* ![Ramon Miramontes photo]({{ site.baseurl }}/images/ramon-miramontes-125x125.jpg)  
* **Presidente**, Ramon Miramontes, MBA

<!--section-->

* ![Jackie Contreras]({{ site.baseurl }}/images/jackie-contreras-125x125.jpg)  
* **Vicepresidente**, Jackie Contreras

<!--section-->

* ![Tara Gomez Hampton]({{ site.baseurl }}/images/tara-gomez-hampton-125x125.jpg)  
* **Secretaria**, Tara Gomez Hampton, Ph.D.  

<!--section-->

* ###Agenda de la Junta Olin:

* [June 30, 2015 Special Board Meeting]({{ site.baseurl }}/docs/Board%20Agenda%206.30.15.pdf)
* [June 11, 2015 Regular Board Meeting]({{ site.baseurl }}/docs/Board%20Agenda%206_11_15%20%282%29-1%20%281%29.pdf)
* [April 29, 2015 Regular Board Agenda]({{ site.baseurl }}/docs/Board%20Agenda%204_29_15B.pdf)
* [April 9, 2015 Special Board Meeting]({{ site.baseurl }}/docs/April_9_2015_Board_Agenda.pdf)
* [January 30, 2015 Special Board Meeting Agenda]({{ site.baseurl }}/docs/Special_Board_Meeting_Agenda-January_30_2015.pdf)
* [December 11, 2014 - Regular Board Meeting Agenda]({{ site.baseurl }}/docs/December%2011%2C%202014%20-%20Regular%20Board%20Meeting%20Agenda.pdf)
* [December 11, 2014 - Special Board Meeting Agenda]({{ site.baseurl }}/docs/December%2011%2C%202014%20-%20Special%20Board%20Meeting%20Agenda%20%40%204%20PM.pdf)
* [October 29, 2014 - Regular Board Meeting Agenda]({{ site.baseurl }}/docs/October%2028%2C%202014%20Regular%20Board%20Meeting%20Agenda.pdf)
* [September 29, 2014 - Special Board Meeting Agenda]({{ site.baseurl }}/docs/September%2029%2C%202014%20Special%20Board%20Meeting%20Agenda.pdf)
* [August 28, 2014 - Regular Board Meeting Agenda]({{ site.baseurl }}/docs/August%2028%2C%202014%20-%20Regular%20Board%20Meeting%20Agenda.pdf)
* [August 21, 2014 - Board Meeting Agenda]({{ site.baseurl }}/docs/August_21_Board_Meeting_Agenda.doc)
* [July 30, 2014 - Special Board Meeting]({{ site.baseurl }}/docs/August_21_Board_Meeting_Agenda.doc)
* [July 15, 2014 - Special Board Meeting]({{ site.baseurl }}/docs/July%2015%20Special%20Board%20Meeting.pdf)
* [July 1, 2014 - Regular Board Meeting]({{ site.baseurl }}/docs/July%201%2C%202014%20-%20Board%20Meeting.pdf)

**Actas:**

* [Regular Board Meeting-December 11,2014-Minutes]({{ site.baseurl }}/docs/Regular%20Board%20Meeting-December%2011%2C2014-Minutes.pdf)
* [Special Board Meeting-December 11,2014_ Minutes]({{ site.baseurl }}/docs/Special%20Board%20Meeting-December%2011%2C2014_%20Minutes.pdf)
* [Special Board Meeting Agenda-January 30, 2015_Minutes]({{ site.baseurl }}/docs/Special_Board_Meeting_Agenda-January_30_2015.pdf)

**Paquetes:**

* [August 21, 2014 Special Board Meeting Packet]({{ site.baseurl }}/docs/August%2021%2C%202014%20Special%20Board%20Meeting%20Packet.pdf)
* [July 30 Official Board Packet]({{ site.baseurl }}/docs/July%2030%20Official%20%20Board%20Packet.pdf)
* [July 15 Board Packet]({{ site.baseurl }}/docs/July%2015%20Board%20Packet.pdf)
* [July 1 Board Agenda]({{ site.baseurl }}/docs/July%201%20Board%20Agenda.pdf)

##Olin LCAP/LCFF
![LCAP-LCFF logo]({{ site.baseurl }}/images/lcff-02-logo-351x121.jpg)
El control local y el plan de rendición de cuentas (LCAP) y la fórmula de financiación de control local (LCFF) una nueva forma para que las escuelas se centren en el éxito de los estudiantes.

* [LACOHS-OLIN Virtual Academy LCAP JUNE 2014.pdf]({{ site.baseurl }}/docs/LACOHS-OLIN_LCAP_JULY_10-2014.pdf)


**Reuniones:**
12 de Junio reunión de maestros LCFF
12 de Junio reunión de padres LCFF

* [LCAP Feedback Survey](https://www.surveymonkey.com/s/MGSVZK3)

![SARC logo]({{ site.baseurl }}/images/sarc-logo-225x225.jpg)
**Informe SARC**  
Las escuelas públicas de California proporcionan anualmente información sobre sí mismo a la comunidad para que el público pueda evaluar y comparar escuelas para rendimiento de los estudiantes, el medio ambiente, los recursos y la demografía.
[Reporte SARC 2012-2013]({{ site.baseurl }}/docs/19642460115337%2012-13%20LA%20County%20Online%20HS%20SARC%20final.pdf)

##Finanzas
[EPA_Website_-_Los_Angeles_County_Online_High_2013-14.pdf]({{ site.baseurl }}/docs/EPA_Website-Los_Angeles_County_Online_High_2013-14.pdf)
