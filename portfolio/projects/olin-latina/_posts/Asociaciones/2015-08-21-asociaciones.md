---
title: Asociaciones
css: md-sub-styles
type: list
image: break-md.jpg
category: Asociaciones
layout: detail
permalink: /asociaciones/
---
###La Academia Hebrew en Huntington Beach

####Premio Académico de "Warm Family Atmosphere"

La Académica Hebrew en Huntington Beach y Olin Virtual Academy se han asociado para crear un programa de aprendizaje mixto a partir de la primavera de 2015. Un pequeño grupo de estudiantes de la Academia Hebrew se matriculo en Olin Virtual Academy tomando en línea cursos académicos mientras el personal de la Academia Hebrew los apoya en sus instalaciones físicas. La Academia Hebrew, por separado de Olin Virtual Academy, proporciona estudios judaicos y otros de aprendizaje en consonancia con el programa de la Academia Hebrew, despues de la escuela. Es esta combinación y la colaboración entre las dos escuelas que proporciona a los estudiantes una excelente oportunidad de aprendizaje.

Visite: [hebrewacademyhb.com](http://www.hebrewacademyhb.com)

###Valle de San Fernando YouthBuild
**Dirrecion:**  
11076 Norris Avenue  
Pacoima, CA 91331  

###Academia Avance
**Dirrecion:**  
115 North Avenue 53  
Highland Park, CA 90042  
