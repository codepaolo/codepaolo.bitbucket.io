---
title: Testimonios de Estudiantes
css: md-sub-styles
type: list
image: group-md.jpg
category: Tesimonios de Estudiantes
layout: detail
permalink: /testimonios/
---

#Testimonios de Estudiatnes

* ![Jessica Gott]({{ site.baseurl }}/images/jessica-gott-125x125.jpg)
* ### Conoce a Jessica Gott
* Mi nombre es Jessica Gott soy la presidenta de la clase del año 2014-2015 en la academia virtual Olin....
* [¡Leer más!]({{ site.baseurl }}/testimonios/jessica-gott/)

<!-- section -->

* ![Perla Zambrano]({{ site.baseurl }}/images/perla-zambrano-125x125.jpg)
* ### Conoce a Perla Zambrano
* Mi nombre es Perla Zambrano, actualmente estoy cursando el grado once. He sido una estudiante en línea por cerca de tres años...
* [¡Leer más!]({{ site.baseurl }}/testimonios/perla-zambrano/)
