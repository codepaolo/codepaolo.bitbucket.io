---
title: El Programa Olin
css: md-sub-styles
type: other
image: el-programa-olin-detail-md.jpg
category: Academicas
layout: detail
permalink: /academicas/programa/
---

## El Programa Olin
Cursos en línea efectivos para moverte hacia delante

* El contenido interactivo es rico en medios de comunicación, involucra activamente a los estudiantes
* Flexibilidad robusta le permite personalizar cursos para satisfacer sus necesidades únicas
* Alineado al básico común y las normas estatales investigación de terceros y estudios
* Premio Académico por haber demostrado la investigación de terceros y estudios
* Contenido de alta calidad para el crédito original, aprendizaje complementario, la recuperación del crédito y el aprendizaje extendido

**100% horario flexible en línea, muy atento a los estudiantes**

* Cursos de recuperación de créditos-gran manera de recuperar las clases a su ritmo
* Cursos electivos
* Grados 9 al 12
* Computadora portátil gratuita, impresora/escáner cuando se registra con el departamento de ayuda técnica
* Servicio de internet reembolsable
* Clases más pequeñas que otras escuelas en línea = más ayuda del profesor
* Software Edmentum/ currículo
* Gran proporción de consejería por estudiante
* Departamento de éxito académico del estudiante para apoyar a los estudiantes con dificultades, además de profesores
