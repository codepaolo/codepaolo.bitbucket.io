---
title: Equipo Estudiantil de Exito Academico
css: md-sub-styles
type: other
image: group-md.jpg
category: Academicas
layout: detail
permalink: /academicas/equipo/
---

##El Equipo Estudiantil de Éxito Académico
El Equipo Estudiantil de Éxito Académico (SAS) se ve compuesto de los educadores con experiencia, altamente calificados que diagnostican, supervisan, apoyan y orientan a todos los estudiantes académicamente. Ellos serán el eje de comunicación entre el estudiante, el maestro, la administración y el hogar cuando los estudiantes no estén cumpliendo con su potencial. La Academia Virtual Olin le asignara un profeso SAS en el momento de la inscripción. Los estudiantes que entran en la “Zona de Apoyo” del Equipo de SAS recibirán, monitoreo constantemente, oportunidades de aprendizaje adicional (intervención) para fortalecer las habilidades de pre-requisitos. Los estudiantes recibirán “Zona de Apoyo” por:

* Falta de reportarse a las clases
* No terminar las tareas
* Promedio bajo en las tareas y evaluaciones
* Alumnos de EL
* Estado de recuperación de créditos
* Estudiantes que estén expandiendo el tiempo del grado doce
* Bajas calificaciones o un promedio de calificaciones 2.0 en el semestre anterior
* La necesidad de intervención se determinara mediante un informe de progreso o calificaciones del semestre
* Estudiantes que no progresan serán colocados en un Plan de Éxito Académico por el Equipo de SAS

Los estudiantes que no estén progresando continuamente en forma académica serán referidos al Director para la evaluación y determinar si el Estudio Independiente es un ajuste inadecuado; y, a partir de tal determinación, puede ser devuelto a su distrito de residencia.

Maestros y personal con credenciales y Cursos aprobados por las Universidades de California.
