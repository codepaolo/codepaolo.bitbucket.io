---
title: Educacion Especial
css: md-sub-styles
type: other
image: educacion-especial-md.jpg
category: Academicas
layout: detail
permalink: /academicas/educacion-especial/
---

#Educación Especial
El departamento de educación especial de Olin Virtual Academy ofrece un continuo apoyo a nuestras familias y estudiantes. El personal está formado por educadores que son expertos en su campo, trabajando con personas que pueden estar en el espectro, tienen problemas de aprendizaje o trastornos emocionales. Nuestros especialistas en la educación también conocidos como administradores de casos son altamente calificados. Además, los maestros de educación especial trabajan en colaboración con los padres, los maestros de educación general, el equipo de éxito académico de los estudiantes, proveedores de servicios DIS (Premio Académico por haber demostrado la investigación de terceros y estudios) y el estudiante.

Nuestra población estudiantil incluye un cuerpo estudiantil diverso que puede tener discapacidades académicas, sociales o emocionales y están en necesidad de ayuda. Planes educativos individualizados están escritos específicos para un estudiante. Valoramos la situación única de cada estudiante y hemos construido una relación con los estudiantes y sus familias. Olin Virtual Academy ofrece servicios que incluyen: servicios de instrucción designados (habla y lenguaje, terapia ocupacional) y DIS que esta individualizado para cada alumno con el trabajo en cada área académica. A los estudiantes que se les presentan los servicios SAI (Premio Académico por haber demostrado la investigación de terceros y estudios) se reúnen con su especialista en educación para centrarse en el apoyo adicional en las habilidades de lectura, escritura, matemáticas, de la universidad, y de transición de carrera y de estudio.

Alojamiento/modificaciones están disponibles para apoyar a un estudiante con el ambiente del aula y el hogar, y se incluye a la hora de tomar la prueba estatal. Este tipo de apoyo esta individualizado IEPs para ayudar a los estudiantes a maximizar su capacidad de tener éxito en el logro de sus metas académicas. Además los maestros de educación especial también trabajan en colaboración con los maestros de educación general para asegurar la aplicación y el nivel de apoyo en el aula ordinaria. Maestros de educación especial también trabajan en colaboración con el equipo de estudiantes de apoyo académico, los proveedores de servicios, los padres y el estudiante.
