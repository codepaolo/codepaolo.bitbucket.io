---
title: Filosofia de la Educacion
css: md-sub-styles
type: other
image: filosofia-md.jpg
category: Academicas
layout: detail
permalink: /academicas/filosofia/
---

#Filosofía de la Educación
La visión de LAOHS sigue siendo ayudar a los estudiantes de secundaria que buscan una experiencia alternativa de la escuela secundaria, en la adquisición de las habilidades necesarias que los equipara y los armara para las etapas posteriores de la vida, y para optimizar el aprendizaje de la vida, y de cada estudiante ayudándoles a alcanzar sus objetivos de aprendizaje individualmente. Esto requiere la comprensión de cada alumno y de su perfil; el diseño de una alta calidad y un programa relevante que da a los estudiantes la mejor oportunidad posible para el éxito académico; y a través de la medición periódica de los progresos respecto a los objetivos. La tecnología ha hecho la personalización de la educación posible. La escuela en línea como tiempo completo no es apropiada para todos los estudiantes. Pero para algunos estudiantes, LACOH puede seguir siendo el punto que los trae de nuevo en el sistema de escuelas públicas:

* Permitiéndole a los estudiantes avanzar a su propio ritmo
* Flexibilidad de horario para acomodar las responsabilidades del trabajo o el cuidado de la familia
* Libertad de ubicación para aquellos que tienen problemas físicos
* Conexiones en un entorno de uno a uno con instructores certificados, para los que necesitan atención personalizada

En LACHOs creemos que el aprendizaje mejor ocurre cuando a los estudiantes se les ofrece una oportunidad que combina un currículo académico riguroso, una fuerte equipo de apoyo con un personal altamente calificado, y las normas y expectativas en un ambiente de aprendizaje de la personalización flexible razonable. LACHOs es instruido para apoyar alto rendimiento de los estudiantes mediante la creación de una alta calidad en línea, con un ambiente personalizado de aprendizaje para todos los estudiantes. Los principios en los cual LACOHs esta diseñado comienza con:

* Las escuelas tradicionales no son para todos los estudiantes
* Todos los estudiantes como individuos deben celebrarse
* Diferentes estudiantes pueden necesitar tomar un camino diferente para lograr el dominio académico y para lograr sus deseos y metas
* La educación debe ser de apoyada en las diferencias de los intereses, habilidades, estilos y capacidades
* Entrega en línea
* La entrega en línea aprovecha la potencia de la era de la informática a profundo, variados contenidos son puestos a disposición a través de internet
* La tecnología en la educación permite la entrega de contenidos educativos que se puede personalizar de manera eficiente a múltiples estilos de aprendizaje, intereses y capacidades
* La tecnología les permite a los estudiantes ser educados de manera eficiente y eficaz a través de un horario flexible en el que puedan aprender en el tiempo, el ritmo, y el lugar de su elección

##Aprendizaje Personalizado
* Las lecciones aprendidas a través del trabajo independiente pueden ser más pegajosas que si se aprendieran a través de conferencias y tienden a quedarse con el alumno más tiempo que los conocimientos adquiridos atravesó de la lectura de un aula
* Students in a personalized learning program develop more effective time-management skills, self discipline,and self-direction.
* Ayuda a los estudiantes de secundaria a construir habilidades para la vida
* Los estudiantes en un programa de aprendizaje de la personalización desarrollan habilidades más eficaces de administración de habilidades, auto-disciplina y auto dirección
* Madurez, administración de habilidades, y ayuda en auto dirección a la carrera o el éxito
* Seguimos creyendo que los adolescentes de hoy en día son capaces de construir relaciones educativas y sociales exitosas a pesar de que trabajan de forma independiente. Sin embargo, también creemos que complementar la experiencia de la escuela secundaria en línea con las interacciones basadas en la comunidad proporcionara poderosos beneficios sociales y académicos para los estudiantes
