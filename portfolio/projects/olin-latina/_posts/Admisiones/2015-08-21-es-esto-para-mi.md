---
title: Es esto para me
css: md-sub-styles
type: other
image: chica-md.jpg
category: Admisiones
layout: detail
permalink: /admisiones/es-esto-para-mi/
---

#¿Es Esto Para Mí?
**Conteste las siguientes preguntas y decide si la Academia Virtual Olin es para ti:**

 1. ¿Te gusta trabajar de forma independiente y a tu propio ritmo?
 2. ¿Estás involucrado en un programa de deportes o en la industria del entretenimiento lo cual requiere viajar?
 3. ¿Te estás quedando atrás en la escuela y tienen miedo de que no te vas a graduarse a tiempo?
 4. ¿Te gustaría graduarte por adelantado y recibir tu diploma de escuela secundaria antes que tus amigos?
 5. ¿Tienes un trabajo y necesita un horario escolar más flexible?
 6. ¿Estás siendo intimidado en la escuela y te gustaría estar en un entorno mas seguro?
 7. ¿Sientes que no encajas en tu escuela y quieres probar algo nuevo?
 8. ¿Otros estudiantes en tu escuela te distraen y te impiden avanzar?
 9. ¿Te gustaría recibir tu diploma de escuela preparatoria en la comodidad de tu propia casa?

#Si usted contestó si a 6 de las preguntas en la lista anterior, entonces esta es la escuela para usted!
**No dude en ponerse en contacto con nosotros si tiene alguna pregunta.**

**Correo Electrónico:** [enrollment@olinacademy.org](mailto:enrollment@olinacademy.org)
**Teléfono:** 1 (800) 985-0770 x1
[!Inscríbete Ahora!]({{ site.baseurl }}/admisiones/)
