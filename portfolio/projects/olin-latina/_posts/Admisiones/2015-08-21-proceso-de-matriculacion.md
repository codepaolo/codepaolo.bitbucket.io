---
title: Proceso de Matriculacion
css: md-sub-styles
type: other
image: cursos-md.jpg
category: Admisiones
layout: detail
permalink: /admisiones/proceso/
---

#Proceso de Matriculación
Olin Virtual Academy requiere que la cuenta de tutor sea creada, antes de comenzar la inscripción. Por favor, tenga los requisitos para el registro de la cuenta:

* Si el estudiante es menor de 18 años de edad, el padre/tutor deberá crear la cuenta de tutor
* Si el estudiante tiene 18 años o más, el estudiante es el titular de la cuenta principal (el tutor no es necesario)

[Crear la Cuenta](https://docs.google.com/forms/d/1h9emHBJKONlrNO_1aTVY7niO0K3tS4p_-V-tkHIED9o/viewform?usp=send_form)

Una vez que la cuenta de tutor haya sido creada, se le pedirá que llene la información de su hijo/a. Una vez que se presente la información, usted recibirá un correo electrónico automatizado que le proporcionara un nombre de usuario y una contraseña temporal.

Con el fin de completar la inscripción de los estudiantes, requerimos los siguientes documentos necesarios (que pueden ser enviados por correo electrónico, por fax o por correo):

* Acta de Nacimiento
* Comprobante de Domicilio (de preferencia una factura de servicios públicos)
* Registros de Vacunación
* Transcripciones del Estudiante
* Solicitud de almuerzo gratis y reducido
* Formulario de consentimiento de las redes sociales de comunicación

**Correo Electrónico:**  
[documents@olinacademy.org](mailto:documents@olinacademy.org)  
**Fax:**  
[(800) 985-0770](tel:800-985-0770)  

**Dirección Postal:**  
2600 Foothill Boulevard, Suite 301  
La Crescenta, CA. 91214  

#La clasificación para el reembolso de internet
Para calificar para el programa de reembolso de internet, usted debe calificar para el programa de almuerzo gratis y reducido Descargue y rellene el formulario requerido. Una vez completado, adjunte el formulario junto con los otros documentos requeridos.

#Formulario de consentimiento para las redes sociales de comunicación
Olin Virtual Academy requiere la autorización y liberación de las formas para su uso cuando los estudiantes, los profesores, el personal, y / o en otros son fotografiados o grabados en un ambiente del grupo pequeño, individual o para ser utilizado con el propósito de los medios de comunicación, marketing, desarrollo y / o servicio al cliente.

#Aprobación de Admisiones
Una vez que haya creado una cuenta y presentado todos los documentos, uno de los consejeros va a crear un horario de clases único para el estudiante. Para recibir la aprobación final de la solicitud, y el Acuerdo de Maestro de Estudios Independientes (ISMA) debe ser firmado (padre / tutor si es menor de 18 años de edad). Después, será recibido por su maestro asignado de Éxito Académico Estudiantil (SAS) y recibirá una computadora portátil y una impresora para comenzar sus estudios en Olin Virtual Academy!
No dude en ponerse en contacto con nosotros en cualquier momento durante el proceso de matriculación.
Correo Electrónico: enrollment@olinacademy.org
**Teléfono:** [1(800) 985-0770 x.1](tel:1-800-985-0770)
