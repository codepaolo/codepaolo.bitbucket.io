---
title: Admisiones
css: md-sub-styles
type: list
image: girl-locker-md.jpg
category: Admisiones
layout: detail
permalink: /admisiones/
description: La Academia Virtual Olin – una escuela secundaria acreditada sin fines de lucro, administrada y operada por educadores.
---

#Admisiones
La Academia Virtual Olin – una escuela secundaria acreditada sin fines de lucro, administrada y operada por educadores.

###La elegibilidad del estudiante
La Academia Olin tiene inscripción de tiempo completo para los estudiantes de noveno a doceavo grado que sean residentes de los siguientes Condados de California:

* Los Angeles County
* Orange County
* San Bernardino County
* Ventura County
* Kern County

<!--
###WASC Accreditation

![WASC Accredation]({{ site.baseurl }}/images/WASC_Logo2012.png)

Attending and graduating from a WASC Accredited school provides your child with the best opportunity to attend and succeed at a 4 year university. The WASC Accreditation process is a rigorous “audit” of the school’s academic program. The WASC approval demonstrates that the school is of high quality. 4 year universities do not accept classes from high schools that are not accredited. Financial Aid is not available to students that graduate from non-accredited schools at the junior college or university level. Graduating from a non-accredited high school will impact applications to Fire, Sheriff, and Police departments plus the United States Armed Forces.
-->
# Inscripción (preguntas frecuentes)

[¡Inscribete Ahora!]({{ site.baseurl }}/admisiones/proceso-de-matriculacion)
