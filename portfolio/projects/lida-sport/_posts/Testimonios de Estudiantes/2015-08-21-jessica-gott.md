---
title: Jessica Gott - Estudiante Actual
css: md-sub-styles
type: list
image:
category: Tesimonios de Estudiantes
layout: detail
permalink: /testimonios/jessica-gott/
---

#Jessica Gott – Estudiante actual

##Presidenta de la clase Estudiantil

![Jessica Gott]({{site.baseurl}}/images/jessica-gott-461x461.jpg){: class="img img-responsive"}

<!--section-->

Mi nombre es Jessica Gott, actualmente soy la presidenta de la clase del año 2014-2015 en Olin Virtual Academy. La parte más agradable de este año escolar ha sido la capacidad de tener una verdadera relación con mis maestros y con el personal educativo así como con otros estudiantes a diferencia de mi escuela en línea anterior.

Mis compañeros y yo tenemos la oportunidad de llegar a conocernos unos a otros a pesar del ambiente virtual; existen pocas limitaciones a través de la pizarra y el apoyo de nuestros profesores. El personal no solo trata de ayudarme a mejorar académicamente, sino también me han ayudado a mejorar mi confianza y motivación personal, tengo un promedio de calificaciones de 4.0.

Olin ofrece la mejor manera de balancear los extracurriculares con académicos debido al horario flexible, beneficioso para universitarios con destino a los estudiantes como yo, que requieren el acceso a las actividades extracurriculares y preparación adecuada para la universidad. Los estudiantes tienen una carrera académica adaptada a sus objetivos haciendo de esta escuela en línea la opción perfecta para cualquier persona que quiera encajar, obtener su diploma de escuela secundaria o que va dirigida a la universidad. Tenemos una voz, oportunidad y sentido de importancia aquí.
