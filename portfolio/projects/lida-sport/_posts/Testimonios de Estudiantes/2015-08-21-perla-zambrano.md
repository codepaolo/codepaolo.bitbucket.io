---
title: Perla Zambrano - Estudiante Actual
css: md-sub-styles
type: list
image:
category: Tesimonios de Estudiantes
layout: detail
permalink: /testimonios/perla-zambrano/
---

#Perla Zambrano- estudiante actual

![Perla Zambrano]({{ site.baseurl }}/images/Perla-Zambrano-256x256.jpg)
Mi nombre es Perla Zambrano, actualmente estoy cursando el grado once. He sido una estudiante en línea por cerca de tres años. Yo siento que el aprendizaje en línea realmente me da tiempo para observar lo que realmente quiero en la vida y meayuda a desarrollar mi pasion por la educación. De ser una estudiante con promedio de calificaciones 0.0 (F) a ser una estudiante con las mejores calificaciones A’s. Actualmente estoy empleada y me compre mi primer coche. Tengo el promedio de calificaciones más alto que he tenido en mi vida y me estoy preparando para ir a la universidad.

El aprendizaje en línea nos da la oportunidad de ser responsables, cada estudiante es diferente pero todos somos muy trabajadores en lo que hacemos. Mucha gente pone el estereotipo de que nosotros no nos esforzamos demaciado. En efecto somos estudiantes, estudiantes reales, tan reales como los estudiantes de escuelas privadas o escuelas públicas.
