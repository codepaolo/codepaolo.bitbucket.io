---
title: Académicas
css: md-sub-styles
type: list
image: study-group-md.jpg
category: Academicas
layout: detail
permalink: /academicas/
description: La Academia Virtual Olin – es una escuela secundaria acreditada sin fines de lucro, administrada y operada por educadores.
---

# Académicas
La Academia Virtual Olin – es una escuela secundaria acreditada sin fines de lucro, administrada y operada por educadores.

<!-- section -->

* ![El programa Olin]({{ site.baseurl }}/images/el-programa-olin-125x125.jpg)
* ### El programa Olin
* Cursos en línea efectivos para moverte hacia delante
* [Programa]({{ site.baseurl }}/academicas/programa/)

<!-- section -->

* ![Filosofía de la Educación]({{ site.baseurl }}/images/filosofia-125x125.jpg)
* ### Filosofía de la Educación
* La misión de LACOHS sigue siendo ayudar a los estudiantes de secundaria que buscan una experiencia de la escuela secundaria alternativa…
* [Filosofía]({{ site.baseurl }}/academicas/filosofia/)

<!-- section -->

* ![Equipo estudiantil de éxito académico]({{ site.baseurl }}/images/equipo-125x125.jpg)
* ### Equipo estudiantil de éxito académico
* El equipo estudiantil de éxito académico está compuesto por educadores altamente calificados con experiencia.
* [Equipo estudiantil de éxito académico]({{ site.baseurl }}/academicas/equipo/)

<!--section-->

* ![Plan de Estudios]({{ site.baseurl }}/images/girl-locker-125x125.jpg)
* ### Plan de Estudios
* Nuevo plan de estudios para el año escolar 2015-2016: Florida Virtual impulsado por Canvas.
* [Plan]({{ site.baseurl }}/academicas/plan/)

<!--section-->

* ![Estudiante de idiomas Inglés]({{ site.baseurl }}/images/estudiante-125x125.jpg)
* ### Estudiante de idiomas Inglés
* La academia virtual Olin es una escuela pública y apoyamos a los estudiantes del idioma ingles al proporcionar instrucción…
* [Idiomas Inglés]({{ site.baseurl }}/academicas/idiomas-ingles/)

<!--section-->

* ![Educación especial]({{ site.baseurl }}/images/educacion-especial-125x125.jpg)
* ### Educación especial
* El departamento de educación especial de la academia virtual Olin ofrece un continuo apoyo a nuestras familias y estudiantes.
* [Educación especial]({{ site.baseurl }}/academicas/educacion-especial/)

<!--section-->

* ![Inscripción simultánea en el colegio comunitario]({{ site.baseurl }}/images/inscripcion-simultanea-125x125.jpg)
* ### Inscripción simultánea en el colegio comunitario
* Estudiantes de la academia virtual en línea tienen la oportunidad de inscribirse en clases en sus colegios comunitarios locales…
* [Inscripcion simultanea en el colegio]({{ site.baseurl }}/academicas/inscripcion/)
