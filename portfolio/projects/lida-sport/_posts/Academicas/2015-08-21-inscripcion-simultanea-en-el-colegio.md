---
title: Inscripcion Simultanea en el Colegio
css: md-sub-styles
type: other
image: inscripcion-simultanea-md.jpg
category: Academicas
layout: detail
permalink: /academicas/inscripcion/
---

#Inscripción simultánea en el colegio comunitario
Los estudiantes de Olin Virtual Academy tienen la oportunidad de inscribirse en clases en sus colegios comunitarios locales para obtener crédito mientras asisten a Olin Virtual Academy. Se les requiere un promedio de calificaciones acumulativo de 2.5 o superior para ser otorgada la inscripción simultánea.
Una vez que los estudiantes completen un curso en el colegio, la escuela podrá darles 10 créditos (el doble de créditos) .  Los estudiantes pueden hablar con un consejero de Olin para consultar las clases que pueden tomar en el colegio, basado en sus necesidades. Los formularios de inscripción concurrentes deben ser obtenidos por en el colegio comunitario local y presentarse con un mínimo de 10 días antes de la fecha límite. Todos los formularios deben ser firmados por los padres/tutor legal, por la consular y aprobados por el director de la escuela o su designado. Los estudiantes deben permanecer matriculados en el número mínimo de cursos requeridos por Olin Virtual Academy para la asistencia de tiempo completo. Es la responsabilidad de los estudiantes de presentar la transcripción con sello al registrado de la escuela para recibir crédito de escuela secundaria para las clases del colegio comunitario.
