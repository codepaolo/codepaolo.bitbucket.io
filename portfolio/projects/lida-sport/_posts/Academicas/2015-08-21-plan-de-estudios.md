---
title: Plan de Estudios
css: md-sub-styles
type: other
image: cursos-md.jpg
category: Academicas
layout: detail
permalink: /academicas/plan/
---

##Nuevo plan de estudios para el año escolar 2015-2016: Florida Virtual impulsado por Canvas

Escuchamos sus comentarios y creamos un comité para determinar qué era lo mejor para nuestros estudiantes e implementaremos estos cambios este verano.

##Nuestro plan de estudios incluye la participación:
* SREB principios esenciales de la calidad y iNACOL estándar de calidad para el conocimiento en línea.
* California, básico común y de las normas nacionales
* Cursos aprobados por más de 70 Universidades de California

##Esto es lo que uno de nuestros estudiantes tenía que decir:
__“!Guau! Vi el video y después de dos minutos, me capturaron. Eso fue todo me convencieron, Canvas es la decisión correcta, si, sin duda, me encanta. ¡Guau!, estoy sinceramente sin palabras. El plan de estudios de Ciencias Físicas, parecía increíble. Ese curso definitivamente no es aburrido en lo absoluto. Las actividades de manos a la obra que mostraron parecían divertidísimas, no voy a mentir. Me hubiera gustado haberme quedado un año más solo para tomar esa clase ya que parecía increíble. Sí, me convencieron”.__

##Áreas del curso
* Artes de leguaje Ingles
* Matemáticas
* Estudios Sociales
* Ciencia
* Educación para la salud física
* Idiomas del mundo
* Bellas Artes
* Carrera/educación técnica

#Los cursos se agrupan por área académica y luego son clasificados según el tipo de curso
**Recuperación de créditos-cursos** que cumplen los requisitos de graduación y están destinados a ayudar a los estudiantes que tratan de recuperar los créditos por el fracaso en cursos anteriores.

**Preparación para la Universidad** los cursos cumplen con los requisitos de graduación y los requisitos de admisión a la universidad.
Este es el plan de estudios estándar ofrecido en la mayoría de los programas de la escuela secundaria. Son apropiados para los estudiantes que están buscando continuar a la universidad y planean introducir una vocación al graduarse con las habilidades académicas que necesitan para triunfar en
la vida.

**Los Cursos de Posición Avanzada** son los cursos más académicamente intensivos diseñados para el estudiante que ha tenido éxito en los requisitos previos del curso, deseos de obtener créditos universitarios mientras están en la escuela y continúan a un colegio o universidad.
Los Cursos Electivos están diseñados para los estudiantes a través de todos los niveles académicos para explorar un interés particular o para profundizar en el conocimiento en un tema específico. Estos cursos cumplen con requisitos de cursos electivos para graduación.
