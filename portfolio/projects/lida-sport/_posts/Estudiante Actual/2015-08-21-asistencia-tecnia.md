---
title: Asistencia Tecnia
css: md-sub-styles
type: other
image:
category: Estudiante Actual
layout: detail
permalink: /estudiante-actual/asistencia-tecnia/
---

#Ayuda para el estudiante
La página de soporte de Tecnología de la Información que ofrece una amplia gama de recursos y materiales de capacitación para ayudarle a “seguir adelante.” Si necesita más información o requiere asistencia técnica, nuestros especialistas de atención al cliente están aquí para ayudarle las 24hors del día durante toda la semana.

**Antes de contactarnos, haga lo siguiente**

* Apague todos los bloqueadores de POP UP
* Instale la versión más reciente de Adobe Flash
* Instale la versión más reciente de Java Runtime
* Instale Adobe Acrobat Reader
* Instale Ad-Aware y haga un análisis completo de su sistema

**Optional Software:**  
* [Instale Libre Office](http://www.libreoffice.org/download/libreoffice-fresh/)
* [Instale Team Viewer](http://www.olinacademy.org/images/downloads/TeamViewerV9.zip)

Después de descargar e instalar estos programas, por favor reinicie el equipo. Si no se resuelven sus problemas no dude en ponerse en contacto con nosotros:

Correo electrónico de soporte técnico: [support@olinacademy.org](support@olinacademy.org)
Los archivos del estudiante: [records@olinacademy.org](records@olinacademy.org)
Teléfono de soporte principal: (800) 985-0770 ext. 201

# Documentos y formularios

* [2014-2015 Olin Academy Course Catalog]({{ site.baseurl }}/docs/2014-2015%20Olin%20Academy%20Course%20Catalog%20Final%207-21-14.pdf)
* [Olin Academy Student Handbook]({{ site.baseurl }}/docs/Board%20Approved%20OVA%20Stu%20HB%209-01-2014.pdf)  
* [Olin Academy Student Handbook (Spanish)]({{ site.baseurl }}/docs/OVA%20Student%20HB%209-01-14%20SPANISH.pdf)  
* [Student Media Release]({{ site.baseurl }}/docs/STUDENT%20MEDIA%20RELEASE.pdf)  
* [Free & Reduced Lunch Application]({{ site.baseurl }}/docs/2014-15%20Eng%20NSLP%20Qualification-Internet%20reimbursement_distributed-1.pdf)  
