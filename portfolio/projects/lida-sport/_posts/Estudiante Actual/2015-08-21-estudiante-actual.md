---
title: Estudiante Actual
css: md-sub-styles
type: list
image: estudiante-md.jpg
category: Estudiante Actual
layout: detail
permalink: /estudiante-actual/
description: La Academia Virtual Olin tiene una gran variedad de clubes y actividades orientadas hacia el interés de los estudiantes.
---

#Estudiante Actual
<!--
##Catálogo de cursos y manual del estudiante
* [2014-2015 Olin Academy Course Catalog]({{ site.baseurl }}/docs/2014-2015%20Olin%20Academy%20Course%20Catalog%20Final%207-21-14.pdf)
* [Olin Academy Student Handbook]({{ site.baseurl }}/docs/Board%20Approved%20OVA%20Stu%20HB%209-01-2014.pdf)
* [Olin Academy Student Handbook (Spanish)]({{ site.baseurl }}/docs/OVA%20Student%20HB%209-01-14%20SPANISH.pdf)
-->
##Calendario de Estudiantes
* [2014-2015 Calendar]({{ site.baseurl }}/docs/2014-2015%20Calendar.pdf)

##Clubs de Estudiantes

###GSA
**Organizador del Club: Rick Dechance**  
1er y 3er Lunes a la 1:00 PM

El propósito de la alianza recta gay (GSA) es crear un ambiente seguro y de apoyo para abordar el aislamiento y problemas que enfrentan nuestras escuelas de población diversa. El (GSA) da la bienvenida a lesbianas, gays, bisexuales, y transexuales estudiantes, los jóvenes interrogados, y sus aliados heterosexuales ...

[Más Información](http://tinyurl.com/olinrdechance)

---
<!--
###Los estudiantes construyen una mejor comunidad (servicio comunitario)
**Organizador del Club: Michelle Almeida**  
1er y 3er Jueves del mes 10:00 AM

We will meet twice a month to discuss service opportunities in the student areas. Students will participate in service projects to enhance their community. They will earn hours of community service that they can document on college applications and scholarship applications.

[Más Información](http://tinyurl.com/olinmalmeida2)

---
-->
###Escritura Creativa
**Organizador del Club: Alison Abrouched**
1er y 3er Martes a las 11:00am

El club de escritura Creativa de la Academia Virtual Olin es un lugar seguro para que los autores se reúnan para compartir, criticar y mejorar su trabajo. Nos reuniremos dos veces al mes para compartir y discutir nuestra propia escritura con el objetivo de ayudar mutuamente a crecer de la mejor manera que podamos.

[Más Información]({{site.baseurl}}/academicas/plan/)

---

###Club Multicultural
**Organizador del Club: Susana Quintero**
2do y 4to Lunes 2 PM - 3 PM

Este club está diseñado para estudiantes que quieren aprender sobre otras culturas de todo el mundo. Cada mes, vamos a elegir un “destino” y aprender sobre su lengua, la cultura, la música, las tradiciones, la comida, et. Juntos vamos a crecer e apreciar otras culturas y compartir nuestra propia cultura.

[Más Información](http://tinyurl.com/OlinSAIQuintero)

---

###Club de STEM
**Organizadores del Club: Club de Kelly Soper, Alice Schmitt**
2do lunes a las 4:00 PM

Vamos a empezar por participar en un evento de codificación de todo el mundo, Hora de Código. A partir de ahí, vamos a ver los diferentes lenguajes de codificación, y las carreras que utilizan esos idiomas. Luego nos pondremos al aprendizaje/práctica a través de Code Academy. Si hay interés, podemos hacer un proyecto más amplio, como la construcción de un sitio web o la creación de un juego.

[Más Información](http://tinyurl.com/OlinSAIQuintero)

---

###ASB/Consejo Estudiantil
**Organizador del club: Elaine Amari, Angela Raines**
Todos los Martes 12 PM

Nos reunimos semanalmente para discutir y planear eventos que fomenten comunidad escolar. Los estudiantes tendrán la oportunidad de servir como representantes y líderes en el establecimiento de esta comunidad y los acontecimientos que les gustaría ver en su escuela.

[Más Información](http://tinyurl.com/olinaraines)
