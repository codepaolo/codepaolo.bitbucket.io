$(function(){
  $('.container-fluid').hide().fadeIn('slow');
  $('#menu-main').hide();
  $('#menu-button').click(function(){
    $('#menu-main').fadeToggle("slow");
      //complete.
  });
});

//Mobile menu panel
$(function(){
  $('#panel-menu').hide();
  $('#menu-btn-mobile').click(function(){
    $('#fa-bars').toggleClass("fa-bars fa-times");
    $('#panel-menu').fadeToggle("slow");
      //complete.
  });
});

//SVG JS fallback
if (!Modernizr.svg) {
  $("img[src$='.svg']")
    .attr("src", fallback);
}

//Active Nav links
$(document).ready(function() {
    $('a[href="' + this.location.pathname + '"]').parent().addClass('active');
});
