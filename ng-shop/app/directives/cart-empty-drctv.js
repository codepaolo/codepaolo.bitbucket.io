AngularFireCart.directive("cartEmpty", function() {
    return{
        restrict: "C",
        template: "<b>Your cart is empty!</b><br/>Let's add some items here."
    };
});